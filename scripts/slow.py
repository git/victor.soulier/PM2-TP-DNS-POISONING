import socket
import signal
import sys
from time import sleep

def shutdown(sig, frame):
    sys.exit(0)


signal.signal(signal.SIGTERM, shutdown)
sin = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sout = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sin.bind(("0.0.0.0", 53))
print("started")

while True:
    buf, client = sin.recvfrom(512)
    print("Fwd")
    sout.sendto(buf, ("10.5.0.4", 53))
    buf = sout.recv(512)
    sleep(3)
    sin.sendto(buf, client)
